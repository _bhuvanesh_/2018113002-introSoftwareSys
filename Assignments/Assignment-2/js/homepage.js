/* Toggle between adding and removing the "responsive" class to topnav when the user clicks on the icon */
function myFunction() {
    var x = document.getElementById("myTopnav");
    if (x.className == "pill-nav") {
      x.className += " responsive";
    } else {
      x.className = "pill-nav";
    }
  }
  var navbar = document.getElementById("myTopnav");
  var sticky = navbar.offsetTop;
  function stickbar() {
    if (window.pageYOffset >= sticky) {
      navbar.classList.add("sticky")
    } else {
      navbar.classList.remove("sticky");
    }
  }   
window.onscroll = function() {stickbar()};

