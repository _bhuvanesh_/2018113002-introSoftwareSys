<style>
    /* Remove the navbar's default margin-bottom and rounded borders */ 
    body{
        height: 1000px;
    }
    .navbar {
      margin-bottom: 10px;
      border-radius: 0;
      font-family: 'Montserrat', sans-serif;
      color : black;
      align-content: center;

    }
    li.active a {
      background-color: rgb(228, 230, 229) !important;
      color: rgb(12, 11, 11) !important;
    }
    nav.navbar li a {
      color:black !important;

    }

    .center-pills {
      display:inline-block;
    }
    
    /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
    .row.content {height: 600px}
    
    /* Set gray background color and 100% height */
    .sidenav {
      padding-top: 20px;
      background-color: white;
      height: 100%;
    }
    
    /* Set black background color, white text and some padding */
    footer {
      background-color: rgb(243, 243, 225);
      color: white;
      padding: 15px;
    }
    
    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .sidenav {
        height: auto;
        padding: 15px;
      }
      .row.content {height:auto;} 
    }

  </style>