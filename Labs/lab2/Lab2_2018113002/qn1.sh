#!/bin/bash

if (($# != 2))
    then
    echo "ERROR : Please enter only 2 arguments. :)"
    exit
fi
let var=$1*$2
echo $var

