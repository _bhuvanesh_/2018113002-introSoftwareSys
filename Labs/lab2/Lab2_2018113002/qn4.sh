#!/bin/bash
nuofin=$#

arr=( "$@" )

for ((i = 0; i<nuofin; i++)) 
do
    
    for((j = 0; j<nuofin-i-1; j++)) 
    do  
        if ((${arr[j]} > ${arr[$((j+1))]})) 
        then 
            t=${arr[$j]} 
            arr[$j]=${arr[$((j+1))]}   
            arr[$((j+1))]=$t 
        fi
    done
done
  

echo ${arr[*]} 


