var data = require('./data.js')
function getHighestMarks() {
    function add(total , num){
        return total + num ;
        
    }
    var arr= [];
    var highName , maxMarks=0 ;
    for (i in data) {
         arr[i] = data[i].reduce(add,0);
         if(arr[i] > maxMarks) {
            highName = i;
            maxMarks = arr[i];
         }
    }
    console.log(highName + " has the highest marks = " + maxMarks);
    return highName;
    
}

function getSubject2Toppers() {
    var arr2=[];
    var arr3=[];
    for (i in data) {
        arr2.push([i,data[i][1]]);
        arr3[i] = data[i][1];
    }
    arr2.sort(function(a,b) {return -a[1]+b[1]} )
    return arr2;

}

console.log(getHighestMarks());

console.log(getSubject2Toppers());
