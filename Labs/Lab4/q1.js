function fifthDay() {
	var today = new Date();
    today = today.getDay();
    var indexer = {1:"Monday" , 2:"Tuesday" , 3:"Wednesday" , 4:"Thursday" ,5:"Friday" , 6:"Saturday" ,7:"Sunday"};
    return indexer[(today+5)%7];
}
