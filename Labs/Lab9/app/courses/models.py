from flask import Flask , render_template
app = Flask(__name__)
 
@app.route("/students/create")
def createPage():
    return render_template("courses/index.html")

if __name__ == "__main__":
    app.run()