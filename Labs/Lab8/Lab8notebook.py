#!/usr/bin/env python
# coding: utf-8

# # Lab Task 8
# # Learning Classes by building a matrix :
# ## S. Bhuvanesh
# ## 2018113002

# In[81]:


class matrix:
    def __init__(self , nrows , ncols , *args):
        self.rows = nrows
        self.cols = ncols 
        if args:
            self.mat = args[0]
        else :
            self.mat = []
            for i in range(nrows*ncols):
                self.mat.append(0)
        
    def __str__(self):
        output = ""
        for i in range(self.rows):
            for j in range(self.cols):
                output += str(self.mat[i*self.cols + j])
                output += " "
            output += "\n"
        return output
    
    def __add__(self,other):
        if (self.rows != other.rows) or (self.cols != other.cols):
            raise Exception("Please only add arrays with the same dimensions")
        sumMat = matrix(self.rows,self.cols)
        for i in range(self.rows*self.cols):
            sumMat.mat[i] = self.mat[i] + other.mat[i]
        return sumMat
    
    def __sub__(self,other):
        if (self.rows != other.rows) or (self.cols != other.cols):
            raise Exception("Please only subtract arrays with the same dimensions")
        sumMat = matrix(self.rows,self.cols)
        for i in range(self.rows*self.cols):
            sumMat.mat[i] = self.mat[i] - other.mat[i]
        return sumMat
    
    def __mul__(self,other):
        if (self.cols != other.rows):
            raise Exception("Please take care of dimensions ")
        sumMat = matrix(self.rows,other.cols)
        for i in range(self.rows):
            for j in range(other.cols):
                temp = 0
                for k in range(self.cols):
                    temp += self.mat[i*self.cols+k] * other.mat[k*other.cols+j]
                sumMat.mat[i*other.cols+j] = temp
                ##LOL This code is changed but error shows different
        return sumMat
            
    def __pow__(self,other):
        if (self.rows != other.rows) or (self.cols != other.cols):
            raise Exception("Please only operate arrays with the same dimensions")
        sumMat = matrix(self.rows,self.cols)
        for i in range(self.rows*self.cols):
            sumMat.mat[i] = self.mat[i] * other.mat[i]
        return sumMat
    
    def __floordiv__(self,other):
        if (self.rows != other.rows) or (self.cols != other.cols):
            raise Exception("Please only operate arrays with the same dimensions")
        sumMat = matrix(self.rows,self.cols)
        for i in range(self.rows*self.cols):
            sumMat.mat[i] = self.mat[i] / other.mat[i]
        return sumMat
    def __lt__(self,other):
        if (self.rows != other.rows) or (self.cols != other.cols):
            raise Exception("Please only operate arrays with the same dimensions")
        sumMat = matrix(self.rows,self.cols)
        for i in range(self.rows*self.cols):
            sumMat.mat[i] = (self.mat[i] < other.mat[i])
        return sumMat
    def __gt__(self,other):
        if (self.rows != other.rows) or (self.cols != other.cols):
            raise Exception("Please only operate arrays with the same dimensions")
        sumMat = matrix(self.rows,self.cols)
        for i in range(self.rows*self.cols):
            sumMat.mat[i] = (self.mat[i] > other.mat[i])
        return sumMat
    def __le__(self,other):
        if (self.rows != other.rows) or (self.cols != other.cols):
            raise Exception("Please only operate arrays with the same dimensions")
        sumMat = matrix(self.rows,self.cols)
        for i in range(self.rows*self.cols):
            sumMat.mat[i] = (self.mat[i] <= other.mat[i])
        return sumMat
    def __eq__(self,other):
        if (self.rows != other.rows) or (self.cols != other.cols):
            raise Exception("Please only operate arrays with the same dimensions")
        sumMat = matrix(self.rows,self.cols)
        for i in range(self.rows*self.cols):
            sumMat.mat[i] = (self.mat[i] == other.mat[i])
        return sumMat
    def __ne__(self,other):
        if (self.rows != other.rows) or (self.cols != other.cols):
            raise Exception("Please only operate arrays with the same dimensions")
        sumMat = matrix(self.rows,self.cols)
        for i in range(self.rows*self.cols):
            sumMat.mat[i] = (self.mat[i] != other.mat[i])
        return sumMat
    def __ge__(self,other):
        if (self.rows != other.rows) or (self.cols != other.cols):
            raise Exception("Please only operate arrays with the same dimensions")
        sumMat = matrix(self.rows,self.cols)
        for i in range(self.rows*self.cols):
            sumMat.mat[i] = (self.mat[i] >= other.mat[i])
        return sumMat
    
    
    def transpose(self):
        output = matrix(self.cols,self.rows)
        for i in range(self.rows):
            for j in range(self.cols):
                output.mat[j*output.cols+i] = self.mat[i*self.cols+j]
        return output
    
    def __getitem__(self,arg):
        r , c = arg
        return self.mat[r*self.cols + c]
    def __setitem__(self,arg,value):
        r , c = arg
        self.mat[r*self.cols + c] = value
    
    


# ### Testing all operations which are valid for matrices:

# In[82]:


mat1 = matrix(2,3,[1,2,3,4,5,6])
mat2 = matrix(3,2,[1,2,3,4,5,6])
print("Matrix 1 : ")
print(mat1)
print("Matrix 2 : ")
print(mat2)


# In[106]:


mat3 = matrix(2,3,[2,2,2,2,2,2])

print("Mat1 + Mat3")
print(mat1+mat3)

print("Mat1 - Mat3")
print(mat1-mat3)

print("Mat1 * Mat2 Matrix Multiplication")
print(mat1*mat2)

print("Mat1 ** Mat3 Elementwise multiplication")
print(mat1**mat3)

print("Mat1 // Mat3 Elementwise division")
print(mat1//mat3)

print("Transpose:")
print(mat1.transpose())

print("Greater than ka test")
print(mat1>mat3)

mat1[1,1]=5
print(mat1[1,1])
mat1[1,1]=7
print(mat1[1,1])


# ### Problems where there is an incorrect input :

# In[107]:


print("adding 2x3 to 3x2")
print(mat1+mat2)


# In[108]:


print("Multiplying 3x2 to 3x2")
print(mat1*mat3)


# In[119]:


import math
class Vector(matrix):
    def __init__(self,nrows,*args):
        super(Vector,self).__init__(nrows,1,*args)
    
    def __str__(self):
        super(Vector,self).__str()
    
    def __getitem__(self,arg):
        arg.append(1)
        super(Vector,self).__getitem__(arg)
    
        
    def __setitem__(self,arg,value):
        arg.append(1)
        super(Vector,self).__getitem__(arg,value)    
    
    def transpose(self):
        raise Exception("Transpose for a vector doesn't make any sense. :( ")
        
    def __mod__(self,other):
        return sum((self**other).mat)
        
    
    def norm(self):
        return math.sqrt(self%self)


# In[120]:


vec1 = Vector(3,[1,2,3])
vec2 = Vector(3,[2,2,2])


# In[122]:


print("Addition")
print(vec1+vec2)

print("Subtraction")
print(vec1-vec2)

print("Dot")
print(vec1%vec2)

print("Transpose will show error")
print(vec1.transpose())


# In[121]:


print(vec1.norm())


# In[ ]:




