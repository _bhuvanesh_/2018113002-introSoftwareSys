def JoinIntegers(lst):
    arr=[]
    for i in lst:
        arr.append(str(i))
    output=''.join(arr)
    assert type(output) is str
    return int(output)


print(JoinIntegers([1,2,3,5,6]))