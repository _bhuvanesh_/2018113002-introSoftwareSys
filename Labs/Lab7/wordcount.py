import sys

try :
    inFile = sys.argv[1]
    outFile = sys.argv[2]
    fin = open(inFile,'r')
    fout = open(outFile,'w')
except IndexError:
    print("Input and output files not detected properly. Please give 2 arguments of input file and output file.")
    exit(1)

inString = fin.read()
inArr = inString.replace(',',' ').replace('.',' ').replace(';',' ').replace(':',' ').replace('.',' ').replace('"',' ').replace('!',' ').replace('?',' ').replace('(',' ').replace(')',' ').replace('\n',' ').split()

words = {}
for i in inArr:
    if i in words:
        words[i] = words[i]+1
    else :
        words[i] = 1


for i in words :
    print(i + ' : ' + str(words[i]))
    fout.write(i + ' : ' + str(words[i]) + '\n')
